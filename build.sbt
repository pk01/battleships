scalaVersion := "2.13.1"

name := "battleships"
organization := "com.battleships"
version := "1.0"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.1" % Test
libraryDependencies += "com.typesafe" % "config" % "1.4.0"
