package battleships

case class Coordinates(row: Char, column: Int)

object Coordinates {
  def parse(input: String): Either[Unit, Coordinates] = {
    if (input.length == 2) {
      val first = input.charAt(0)
      val second = input.charAt(1)

      if (first.isLetter && Board.Rows.contains(first) && second.isDigit && second.asDigit <= Board.BoardSize)
        Right(Coordinates(first, second.asDigit))
      else
        Left(())
    }
    else
      Left(())
  }
}
