package battleships

import scala.util.Random

sealed trait ShotType
case object Hit extends ShotType
case object Miss extends ShotType
case class Shot(`type`: ShotType, coordinates: Coordinates)

object Shots {
  def randomizedShot(ships: Seq[Ship]): Coordinates =
    ships.flatMap(_.coordinates)(Random.nextInt(ships.flatMap(_.coordinates).size))
}
