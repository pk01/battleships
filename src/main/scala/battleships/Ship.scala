package battleships

import scala.annotation.tailrec
import scala.util.Random

case class Name(value: String)
case class Hits(coordinates: Set[Coordinates])
case class Width(value: Int)
case class Ship(name: Name, width: Width, coordinates: Seq[Coordinates] = Nil, hits: Hits = Hits(Set.empty))

object Ships {

  def isSunk(ship: Option[Ship]): Boolean =
    ship.fold(false)(ship => ship.hits.coordinates == ship.coordinates.toSet)

  @tailrec
  def randomlyPlacedShips(boardSize: Int, rows: Seq[Char], ships: Seq[Ship] = ships): Seq[Ship] = {
    val placedShips = ships.map(ship => ship.copy(coordinates = generateCoordinates(ship, boardSize, rows)))
    val coordinates = placedShips.map(_.coordinates)

    if (coordinates.flatten.distinct.size != coordinates.flatten.size)
      randomlyPlacedShips(boardSize, rows, ships)
    else
      placedShips
  }

  private def generateCoordinates(ship: Ship, boardSize: Int, rows: Seq[Char]): Seq[Coordinates] = {
    val row = rows(Random.nextInt(boardSize))
    val column = Random.nextInt(boardSize - ship.width.value)

    (column until column + ship.width.value)
      .map(col => Coordinates(row, col))
  }

  private def ships: Seq[Ship] =
    Seq(
      Ship(Name("Carrier"), Width(5)),
      Ship(Name("Battleship"), Width(4)),
      Ship(Name("Cruiser"), Width(3)),
      Ship(Name("Submarine"), Width(3)),
      Ship(Name("Destroyer"), Width(2))
    )
}
