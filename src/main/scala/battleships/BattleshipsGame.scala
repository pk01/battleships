package battleships

object BattleshipsGame {
  def main(args: Array[String]): Unit = {
    val human = Player(Ships.randomlyPlacedShips(Board.BoardSize, Board.Rows))
    val computer = Enemy(Ships.randomlyPlacedShips(Board.BoardSize, Board.Rows))

    Board.showBoards(human.ships, human.shots)
    GameState.gameLoop(human, computer)
  }
}
