package battleships

import scala.Console.println

sealed trait Participant
case class Player(ships: Seq[Ship], shots: Seq[Shot] = Nil) extends Participant
case class Enemy(ships: Seq[Ship], shots: Seq[Shot] = Nil) extends Participant

object Participants {
  def shoot[Source, Target](targetShips: Target => Seq[Ship], addShot: (Source, Shot) => Source, addHit: (Target, Ship, Coordinates) => Target)
                           (source: Source, coord: Coordinates, target: Target): (Source, Target) = {
    val (shot, shotTarget) = targetShips(target).find(_.coordinates.contains(coord)) match {
      case Some(ship) =>
        println(s"Successful hit at ${coord.row}${coord.column}!")

        val opponent = addHit(target, ship, coord)
        if (Ships.isSunk(targetShips(opponent).find(_.coordinates.contains(coord))))
          println(s"${ship.name.value} is sunk!")

        (Shot(Hit, coord), opponent)
      case None =>
        println("Miss!")
        (Shot(Miss, coord), target)
    }

    (addShot(source, shot), shotTarget)
  }
}
