package battleships

import scala.Console.println
import scala.annotation.tailrec

sealed trait Continue
case object PlayerLost extends Continue
case object EnemyLost extends Continue
case object Draw extends Continue
case object GoOn extends Continue

object GameState {

  @tailrec
  def gameLoop(p: Player, e: Enemy, continue: Continue = GoOn): (Player, Enemy, Continue) = {
    println("Enter coordinates to shoot at: (e.g. A1): ")
    val input = scala.io.StdIn.readLine()

    val (player, enemy, _) = Coordinates.parse(input) match {
      case Right(value) =>
        val (playerState, enemyState) = gameRound(value, p, e)
        (playerState, enemyState, GoOn)
      case Left(_) =>
        println("Invalid coordinates!")
        (p, e, GoOn)
    }

    checkContinue(player, enemy) match {
      case GoOn =>
        Board.showBoards(player.ships, player.shots)
        gameLoop(player, enemy, GoOn)
      case Draw =>
        println("It was a draw!")
        (player, enemy, Draw)
      case PlayerLost =>
        println("You lost!")
        (player, enemy, PlayerLost)
      case EnemyLost =>
        println("You won!")
        (player, enemy, EnemyLost)
    }
  }

  private def gameRound(input: Coordinates, p: Player, e: Enemy): (Player, Enemy) = {
    println("Your shot: ")
    val (player, enemy) = Participants.shoot[Player, Enemy](
      target => target.ships,
      (target, shot) => target.copy(shots = target.shots :+ shot),
      (target, ship, crds) => target.copy(ships = target.ships.updated(target.ships.indexOf(ship), ship.copy(hits = Hits(ship.hits.coordinates + crds))))
    )(p, input, e)

    println("Opponent's shot: ")
    Participants.shoot[Enemy, Player](
      target => target.ships,
      (target, shot) => target.copy(shots = target.shots :+ shot),
      (target, ship, crds) => target.copy(ships = target.ships.updated(target.ships.indexOf(ship), ship.copy(hits = Hits(ship.hits.coordinates + crds))))
    )(enemy, Shots.randomizedShot(player.ships), player)
      .swap
  }

  private def checkContinue(player: Player, enemy: Enemy): Continue = {
    val playerLost = player.ships.forall(ship => ship.hits.coordinates == ship.coordinates.toSet)
    val enemyLost = enemy.ships.forall(ship => ship.hits.coordinates == ship.coordinates.toSet)

    if (playerLost && enemyLost) Draw
    else if (playerLost) PlayerLost
    else if (enemyLost) EnemyLost
    else GoOn
  }
}
