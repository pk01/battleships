package battleships

import com.typesafe.config.ConfigFactory

import scala.Console.println

sealed trait TileType

case object EmptyTile extends TileType

case object TakenTile extends TileType {
  override def toString = "[**]"
}

case object HitTile extends TileType {
  override def toString = "[XX]"
}

case object MissTile extends TileType {
  override def toString = "[--]"
}

case class Tile(`type`: TileType, coordinates: Coordinates) {
  override def toString = if (`type` == EmptyTile) s"[${coordinates.row}${coordinates.column}]" else `type`.toString
}

object Tiles {
  def from(shot: Shot): Tile = shot match {
    case Shot(Miss, coordinates) => Tile(MissTile, coordinates)
    case Shot(Hit, coordinates) => Tile(HitTile, coordinates)
  }
}

object Board {
  type Board = Seq[Seq[Tile]]
  val BoardSize: Int = ConfigFactory.load().getInt("boardSize")
  val Rows: Seq[Char] = ('A' to 'Z').take(BoardSize)

  def showBoards(ships: Seq[Ship], shots: Seq[Shot]): Unit = {
    val shipBoard = createShipBoard(ships)
    val shootingBoard = createShootingBoard(shots)

    println("Your ships:                              Taken shots:")
    shipBoard.zip(shootingBoard).foreach { case (shipTile, shotTile) => shipTile foreach print; print(" "); shotTile foreach print; println }
  }

  def createShipBoard(ships: Seq[Ship]): Board = {
    val shipTiles = Board.fillBoard(ships.flatMap(_.coordinates).map(Tile(TakenTile, _)))
    Board.fillBoard(ships.flatMap(_.hits.coordinates).map(Tile(HitTile, _)), shipTiles)
  }

  def createShootingBoard(shots: Seq[Shot]): Board =
    Board.fillBoard(shots.map(Tiles.from))

  private def fillBoard(tiles: Seq[Tile], board: Board = emptyBoard): Board =
    tiles.foldRight(board)((tile, board) => place(tile, board))

  private def emptyBoard: Board =
    Rows.map(row => (0 until BoardSize).map(column => Tile(EmptyTile, Coordinates(row, column))))

  private def place(tile: Tile, board: Board): Board = {
    val rowIndex = Rows.indexOf(tile.coordinates.row)
    val tiles: Seq[Tile] = board(rowIndex).patch(tile.coordinates.column, Seq(tile), 1)

    board.updated(rowIndex, tiles)
  }
}
