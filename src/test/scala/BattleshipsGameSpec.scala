import java.io.ByteArrayInputStream

import battleships._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class BattleshipsGameSpec extends AnyFlatSpec with Matchers {

  "game" should "end with a draw" in {
    Console.withIn(new ByteArrayInputStream("A1".getBytes)) {
      val (player, enemy, result) = GameState.gameLoop(Player(Seq(Ship(Name("Player"), Width(1), Seq(Coordinates('A', 0))))),
        Enemy(Seq(Ship(Name("Enemy"), Width(1), Seq(Coordinates('A', 1))))))

      player.ships.flatMap(_.hits.coordinates).headOption shouldBe Some(Coordinates('A', 0))
      player.shots.map(_.coordinates).headOption shouldBe Some(Coordinates('A', 1))

      enemy.ships.flatMap(_.hits.coordinates).headOption shouldBe Some(Coordinates('A', 1))
      enemy.shots.map(_.coordinates).headOption shouldBe Some(Coordinates('A', 0))

      result shouldBe Draw
    }
  }

  "game" should "end with player losing" in {
    Console.withIn(new ByteArrayInputStream("C5".getBytes)) {
      val (player, enemy, result) = GameState.gameLoop(Player(Seq(Ship(Name("Player"), Width(1), Seq(Coordinates('A', 0))))),
        Enemy(Seq(Ship(Name("Enemy"), Width(1), Seq(Coordinates('A', 1))))))

      player.ships.flatMap(_.hits.coordinates).headOption shouldBe Some(Coordinates('A', 0))
      player.shots.map(_.coordinates).headOption shouldBe Some(Coordinates('C', 5))

      enemy.ships.flatMap(_.hits.coordinates).headOption shouldBe None
      enemy.shots.map(_.coordinates).headOption shouldBe Some(Coordinates('A', 0))

      result shouldBe PlayerLost
    }
  }

  "game" should "end with enemy losing" in {
    Console.withIn(new ByteArrayInputStream("A0".getBytes)) {
      val (player, enemy, result) = GameState.gameLoop(Player(Seq(Ship(Name("Destroyer"), Width(2), Seq(Coordinates('A', 0), Coordinates('A', 1))))),
        Enemy(Seq(Ship(Name("Enemy"), Width(1), Seq(Coordinates('A', 0))))))

      player.ships.flatMap(_.hits.coordinates) should not equal Some(Seq(Coordinates('A', 0), Coordinates('A', 1)))
      player.shots.map(_.coordinates).headOption shouldBe Some(Coordinates('A', 0))

      enemy.ships.flatMap(_.hits.coordinates).headOption shouldBe Some(Coordinates('A', 0))

      result shouldBe EnemyLost
    }
  }

  "coordinate parsing" should "not allow invalid data" in {
    Coordinates.parse("42") shouldBe Left(())
  }

  "coordinate parsing" should "allow valid data" in {
    Coordinates.parse("A0") shouldBe Right(Coordinates('A', 0))
  }

  "ship coordinates" should "not overlap" in {
    val coordinates = Ships.randomlyPlacedShips(Board.BoardSize, Board.Rows).flatMap(_.coordinates)

    coordinates.distinct.size shouldBe coordinates.size
  }

  "board" should "have appropriately placed ships" in {
    val ships = Seq(Ship(Name("Test"), Width(1), Seq(Coordinates('A', 1))))
    val shipBoard = Board.createShipBoard(ships)
    shipBoard.head(1).`type` shouldBe TakenTile
  }

  "board" should "have appropriately placed damaged ships" in {
    val ships = Seq(Ship(Name("Test"), Width(1), Seq(Coordinates('A', 1)), Hits(Set(Coordinates('A', 1)))))
    val shipBoard = Board.createShipBoard(ships)
    shipBoard.head(1).`type` shouldBe HitTile
  }

  "board" should "have appropriately placed successful shots" in {
    val player = Player(Nil, Seq(Shot(Hit, Coordinates('A', 1))))
    val shootingBoard = Board.createShootingBoard(player.shots)
    shootingBoard.head(1).`type` shouldBe HitTile
  }

  "board" should "have appropriately placed missed shots" in {
    val player = Player(Nil, Seq(Shot(Miss, Coordinates('A', 1))))
    val shootingBoard = Board.createShootingBoard(player.shots)
    shootingBoard.head(1).`type` shouldBe MissTile
  }

  "ship" should "be assumed to be sunk" in {
    val ship = Ship(Name("Test"), Width(1), Seq(Coordinates('A', 1)), Hits(Set(Coordinates('A', 1))))
    Ships.isSunk(Some(ship)) shouldBe true
  }

  "ship" should "be assumed to not be sunk" in {
    val ship = Ship(Name("Test"), Width(1), Seq(Coordinates('A', 1), Coordinates('A', 2)), Hits(Set(Coordinates('A', 1))))
    Ships.isSunk(Some(ship)) shouldBe false
  }
}
